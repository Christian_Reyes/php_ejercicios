<?php
//Christian Antonio Reyes Barrera
echo "<center>";
echo "PIRAMIDE<br /><br />";
for ($i=0; $i < 30; $i++) { 
    for ($j=0; $j < $i; $j++) { 
        echo "*";
    }
    echo "<br />";
}

echo "<br /><br />ROMBO<br /><br />";
for ($i=0; $i <= 60; $i++) { 
    $j = 0;
    if($i <= 30)
        $k = $i;
    else
        $k = 30 - ($i - 30);

    while ($j <= $k) {
        echo "*";
        $j++;
    }
    echo "<br />";
}
?>