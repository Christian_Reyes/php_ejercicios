<?php 
//Realizar una expresión regular que detecte emails correctos.

$correo = 'christian_arb@hotmail.com';
$result = preg_match('/[a-zA-Z1-9._%+-]+@[a-zA-Z1-9.]/',$correo);
echo $result;
echo '<br />';

//Realizar una expresion regular que detecte Curps Correctos
//ABCD123456EFGHIJ78.

$curp = 'CARB910328HFGHIJ78';
$result = preg_match('/^[a-zA-Z]{4}[0-9]{2}[0-1][0-9][0-9]{2}[H|M][a-zA-Z]{5}[a-zA-Z0-9][0-9]$/i',$curp);
echo $result;
echo '<br />';

//Realizar una expresion regular que detecte palabras de longitud mayor a 50
//formadas solo por letras.

$palabra = 'ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ';
$result = preg_match('/^[a-zA-Z(^0-9)]{50,}/i',$palabra);
echo $result;
echo '<br />';
//Crea una funcion para escapar los simbolos especiales.

$ejemplo = '$este es un ^ejemplo/';
$result = preg_quote($ejemplo,'/');
echo $result;
echo '<br />';

//Crear una expresion regular para detectar números decimales.
$numero = '132.55';
$result = preg_match('/^[0-9]+\.[0-9]*$/',$numero);
echo $result;
?>