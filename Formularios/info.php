<?php session_start();  
    if(!empty($_GET['c'])){
        $_POST['cuenta'] = $_SESSION['Usuarios'][$_GET['c']]['num_cta'];
        $_POST['pass'] = $_SESSION['Usuarios'][$_GET['c']]['contrasena'];
    }
    if(empty($_POST['cuenta']) || empty($_POST['pass'])){
        echo "<script>alert('Debe indicar usuario y contraseña para continuar')</script>";
        header('Location: login.php');
    }else{
        foreach($_SESSION['Usuarios'] as $llave => $arrayUser){
            if($_POST['cuenta'] == $arrayUser['num_cta']){
                if($_POST['pass'] == $arrayUser['contrasena']){
                    $specialKey = $llave;
                    break;
                }else{
                    echo "<script>alert('Usuario o contraseña incorrectos. Favor de verificar')</script>";
                    header('Location: login.php');
                }
            }else if(end($_SESSION['Usuarios']) == $arrayUser){
                echo "<script>alert('Usuario o contraseña incorrectos. Favor de verificar')</script>";
                header('Location: login.php');
            }
        }
    }
    ?>
<html>
	<head>
		<title> INFO </title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/info-styles.css">
	</head>
	<body>
        <div class="barra-inicial">
            <div class="item-barra">
                <a href="./info.php?&c=<?php echo $specialKey?>">Home</a>
            </div>
            <div class="item-barra">
                <a href='./formulario.php?&c=<?php echo $specialKey?>'>Registrar Alumnos</a>
            </div>
            <div class="item-barra">
                <a href='./login.php'>Cerrar Sesión</a>
            </div>
        </div>
        <br/>
        <h1>Usuario Autenticado</h1>
        <div class="container-infoName">
            <div class="container-nombre">
                <p class="nombre-usuario"><?php 
                        echo $arrayUser['nombre']." ".$arrayUser['primer_apellido'];
                    ?>
                </p>
            </div>
            <div class="container-info">
                <p>Información</p>
                <p>Número de cuenta: <?php echo $arrayUser['num_cta']?></p>
                <p>Fecha de nacimiento: <?php echo $arrayUser['fecha_nac']?></p>
            </div>
        </div>
        <br/><br/>
        <h1>Datos guardados:</h1>
        <div class="container-save">
            <p>#</p>
            <p>Nombre</p>
            <p>Fecha de Nacimiento</p>
            <?php 
                foreach($_SESSION['Usuarios'] as $key => $element){
                    echo '<p>'.$element['num_cta'].'</p>';
                    echo '<p>'.$element['nombre']." ".$element['primer_apellido']." ".$element['segundo_apellido'].'</p>';
                    echo '<p>'.$element['fecha_nac'].'</p>';
                }
            ?>
        </div>
    </body>
</html>