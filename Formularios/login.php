<?php session_start(); 
    if(empty($_SESSION)){
        $_SESSION['Usuarios'] = [
            1 => [
                'num_cta' => '1',
                'nombre' => 'Admin',
                'primer_apellido' => 'General',
                'segundo_apellido' => '',
                'contrasena' => 'adminpass123',
                'genero' => 'O',
                'fecha_nac' => '1990-01-01'
            ],
            2 => [
                'num_cta' => '2',
                'nombre' => 'Christian',
                'primer_apellido' => 'Reyes',
                'segundo_apellido' => 'Barrera',
                'contrasena' => 'c321',
                'genero' => 'M',
                'fecha_nac' => '1991-02-28'
            ]
        ];
    }
?>
<html>
<head>
    <meta charset="UTF-8">
	<title>LOGIN</title>
    <link rel="stylesheet" href="css/login-styles.css">
</head>
    <body>
    <div class = "container-all">
        <div class = "container">
            <h2>Login</h2>
            <form action="info.php" method="POST">
                <label class="form-label">Número de cuenta:</label>
                
                <input name="cuenta" class="form-input" type="text" id="input-cuenta">
                
                <label class="form-label">Contraseña:</label>
                
                <input name="pass" class="form-input" type="password" id="input-pass">
                
                <input type="submit" class="btn" value="Entrar">
            </form>
        </div>
    </div>
    </body>
</html>