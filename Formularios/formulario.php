<?php session_start();
if(!empty($_POST)){
    foreach($_SESSION['Usuarios'] as $key => $element){
        if(end($_SESSION['Usuarios']) == $element){
        break;
        }
    }
    $key++;
    $_SESSION['Usuarios'][$key]['num_cta'] = $_POST['Cuenta'];
    $_SESSION['Usuarios'][$key]['nombre'] = $_POST['txtNombre'];
    $_SESSION['Usuarios'][$key]['primer_apellido'] = $_POST['txtApPat'];
    $_SESSION['Usuarios'][$key]['segundo_apellido'] = $_POST['txtApMat'];
    $_SESSION['Usuarios'][$key]['genero'] = $_POST['opgenero'];
    $_SESSION['Usuarios'][$key]['fecha_nac'] = $_POST['date'];
    $_SESSION['Usuarios'][$key]['contrasena'] = $_POST['passR'];  
    echo "<script>alert('Usuario Registrado exitosamente')</script>";
}
if(empty($_REQUEST['c'])){
    header('Location: login.php');
}
/*<h2><?php echo "Hola {$_SESSION['Usuarios'][$llave]['nombre']} <br />"; ?></h2>
        <h2><?php print_r($_REQUEST); ?></h2>
        <h2><?php echo $_REQUEST['c']; ?></h2>*/
?>
<html>
	<head>
		<title> INFO </title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/formulario-styles.css">
	</head>
	<body>
        <div class="barra-inicial">
            <div class="item-barra">
                <a href='./info.php?&c=<?php echo $_REQUEST['c']?>'>Home</a>
            </div>
            <div class="item-barra">
                <a href='./formulario.php?&c=<?php echo $_REQUEST['c']?>'>Registrar Alumnos</a>
            </div>
            <div class="item-barra">
                <a href='./login.php'>Cerrar Sesión</a>
            </div>
        </div>
        <br/><br/><br/>
        <div class="container-form">
            <form class="form-alumno" action="formulario.php?&c=<?php echo $_REQUEST['c']?>" method="POST">
                <label class="form-label"> Número de cuenta:</label>
                <input type="text" name="Cuenta" class="form-input"></input>
                <br/>
                <label class="form-label">Nombre:</label>
                <input type="text" name="txtNombre"></input>
                <br/>
                <label class="form-label">Primer Apellido:</label>             
                <input type="text" name="txtApPat"></input>
                <br/>
                <label class="form-label">Segundo Apellido:</label>                
                <input type="text" name="txtApMat"></input>
                <br/>
                <label class="form-label">Genero</label>                
                
                <div>
                    <input type="radio" name="opgenero" value="M"> Masculino </input> <br>
                    <br/>
                    <input type="radio" name="opgenero" value="F"> Femenino </input> <br>
                    <br/>
                    <input type="radio" name="opgenero" value="O"> Otro </input> <br>
                </div>
                <br/>
                <label class="form-label">Fecha de nacimiento</label>
                <input name="date" class="form-input" type="date" id="input-date"
					   placeholder="fecha">
                <br/>
                <label class="form-label">Contraseña</label>                
                <input type="password" class="passRegistrar" name="passR"></input>
                <br/>
                <label> </label>
                <input type="submit" name="RegistrarAlumno"  value="Registar" class="btnRegistrar"></input>
            </form>    
        </div>
    </body>
</html>
